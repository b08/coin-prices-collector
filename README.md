# @b08/library-seed
This is a gen3 seed for typescript library.

# Features
1. Gulp tasks include building, linting, testing and measuring coverage.
2. Gulp tasks are extracted to @b08/library-tasks package.
3. npx is used in all tasks, so no global packages need to be installed.
4. Along with building for latest target (esnext), there are tasks to build the project for 2 old targets - ES5 and ES6.\
   To import from one of those targets, use the corresponding suffix, like the following: import { maxBy } from '@b08/array/es5'
5. Gitlab CI script will do full test and coverage check on any merge request.
6. When MR is merged into master, Gitlab CI script will publish npm package, built off that commit, and then create version tag and post metrics to the output for collector to gather.\
   Version will be taken from package.json, but only if it is higher than latest published version, i.e. if it was set manually to higher value.\
   Otherwise latest published version will be incremented and will be used for next publish.
7. Following metrics are collected - LOC, number of tests and coverage.

# Debugging tests
Run any of your test files directly from VSCode "debug test" launch option.\
Can also try and start gulp task testInVs.

# Creating a typescript library from this seed
Use @b08/seed-that, to start your library, refer to its readme to create settings.

environment variables needed in gitlab CI/CD settings:\
gitKey - private git key, needed to push git tags\
npmToken - needed to publish library to npmRepository\
